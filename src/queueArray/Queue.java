/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queueArray;

/**
 *
 * @author Alif Jafar
 */
public class Queue {
    
    private int size, front, rear, nItems;
    private String[] queue;
    
    public Queue(int s){
        size = s;
        queue = new String [size];
        front = 0;
        rear = -1;
        nItems = 0;
    }
    
    public void insert(String i){
        if(rear == queue.length-1){
            rear = -1;
        }
        
        queue[++rear] = i;
        nItems++;
    }
    
    public String remove(){
        String temp = queue[front++];
        if(front == queue.length){
            front = 0;
        }
        nItems--;
        return temp;
    }
    
    public String peekFront() {
        return queue[front];
    }
    
    public boolean isEmpty(){
        return (nItems==0);
    }
    
    public boolean isFull(){
        return (nItems == queue.length);
    }
    
    public int size(){
        return nItems;
    }
}
