/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package queueArray;

import java.util.Scanner;

/**
 *
 * @author Alif Jafar
 */
public class QueueApp {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Masukan Size Queue : ");
        int size = input.nextInt();
        Queue q = new Queue(size);
        input.nextLine();
        System.out.println("\n==Enqueue Queue==");
        for (int i = 0; i < size; i++) {
            System.out.print("Masukan Antrian ke " + (i + 1) + " : ");
            String antrian = input.nextLine();
            q.insert(antrian);

        }
        System.out.println("");
        System.out.println(q.remove()+ " Telah di remove");
        System.out.println(q.remove()+ " Telah di remove");
        System.out.println(q.remove()+ " Telah di remove");
        
        System.out.println("Size Queue : "+q.size());
        System.out.println("Peek Front : "+q.peekFront());

        System.out.println("\n==Isi Queue :==");
        while (!q.isEmpty()) {
            String n = q.remove();
            System.out.print(n);
            System.out.print(" ");
        }
        System.out.println();
    }

}
