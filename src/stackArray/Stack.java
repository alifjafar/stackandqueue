/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stackArray;

/**
 *
 * @author Alif Jafar
 */
public class Stack {

    private int size, top, cap;
    private int[] stack;

    public Stack(int s) {
        size = s;
        stack = new int[size];
        top = -1;
    }

    public void push(int p) {
        if (!isFull()) {
            stack[++top] = p;
            cap = top;
        } else {
            System.out.println("Stack Penuh");
        }
    }

    public int pop() {
        if (isEmpty()) {
            System.out.println("Stack Kosong");
            return 0;
        }
        cap -=1;
        return stack[top--];
    }

    public int top() {
        return stack[top];
    }

    public boolean isEmpty() {
        return (top == -1);
    }

    public boolean isFull() {
        return (top == stack.length - 1);
    }

    public int getSize() {
        return (top+1);
    }

    public void viewStack() {
        if(!isEmpty()){
            System.out.println("\n==Isi Stack==");
            while(!isEmpty()){
                int res = stack[top--];
                System.out.print(res +" ");
            }
        }
        else {
            System.out.println("Stack Kosong");
        }
        System.out.println("");
        top = cap;

    }
}
