/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stackArray;

import java.util.Scanner;

/**
 *
 * @author Alif Jafar
 */
public class StackApp {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("Masukan Size Stack : ");
        int size = input.nextInt();
        System.out.println("[i] Size Stack adalah : "+size);
        Stack s = new Stack(size);
        
        System.out.println("\n==Push Stack==");
        for(int i=0; i < size; i++){
            System.out.print("Masukan Tumpukan ke "+(i+1)+" : ");
            int angka = input.nextInt();
            s.push(angka);
            
        }
        
        s.viewStack();
        System.out.println("\n[i] Top Stack adalah : " +s.top());
        System.out.println(s.pop()+" Telah di Pop");
        System.out.println("\n[i] Size Stack : " +s.getSize());
        s.viewStack();
        
    }
    
}
